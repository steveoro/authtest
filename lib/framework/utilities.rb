=begin

= Utilities module

  - version:  1.00.001
  - author:   Steve A.

  Collection of generic & reusable methods.
=end
module Utilities

  # Default keys removed by #strip_hash_members()
  DEFAULT_FORBIDDEN_KEYS = ['password', 'password_confirmation']


  # Returns the same specified Hash with any password-related field stripped away.
  #
  # For debugging/logging purposes.
  #
  # Works only with a 0-th & single-depth nesting hashes (since this implementation
  # is not recursive). As in:
  #
  #   params['password'] or
  #   params['anything']['password']
  #
  def self.strip_hash_members( hash, forbidden_keys = DEFAULT_FORBIDDEN_KEYS )
    result_hash = hash.respond_to?(:to_hash) ? hash.to_hash : {}
    result_hash = result_hash.reject { |key|
      forbidden_keys.include?(key.to_s)
    }
    result_hash.each { |key, value|
      if value.instance_of?( Hash )
        value.reject! { |sub_key, sub_value|
          forbidden_keys.include?(sub_key.to_s)
        }
      end
    }
    result_hash
  end
  #-- -------------------------------------------------------------------------
  #++
end
