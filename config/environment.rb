# Load the Rails application.
require File.expand_path('../application', __FILE__)

require 'framework/version'


#--
# -- AgeX-specific stuff: --
#++

# Current AgeX Framework version
AGEX_FRAMEWORK_VERSION  = Version::FULL unless defined? AGEX_FRAMEWORK_VERSION

# App-name, lowercase, used in shared modules or sub-projects
AGEX_APP                = 'authtest' unless defined? AGEX_APP

# "Displayable" App-name, used in shared modules or sub-projects
AGEX_APP_NAME           = 'AuthTest' unless defined? AGEX_APP_NAME

# Logo image file for this App, used in shared modules or sub-projects
AGEX_APP_LOGO           = 'application_key.png' unless defined? AGEX_APP_LOGO

# Accepted and defined locales
LOCALES = {'en' => 'en-US', 'it' => 'it-IT'}.freeze
I18n.enforce_available_locales = true
# ---------------------------------------------------------------------------

# Initialize the Rails application.
Authtest::Application.initialize!
