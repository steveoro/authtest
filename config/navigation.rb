# -*- coding: utf-8 -*-
# Configures your navigation
SimpleNavigation::Configuration.run do |navigation|
  # Define the primary navigation
  navigation.items do |primary|
    primary.item( :key_home,           t('home'), '#' ) do |lev2_nav|
      lev2_nav.item :key_main,         t('main'), root_path()
      lev2_nav.item :key_separator0_1, content_tag(:span, '' ), class: 'divider'
      lev2_nav.item :key_main,         t('articles.articles'),  articles_path(), if: Proc.new{ user_signed_in? }
      lev2_nav.item :key_misc,         t('more_to_come'),  '#', class: 'disabled'
    end

    primary.item :key_separator1,      '&nbsp;', '#', class: 'disabled'
    primary.item(
      :key_user_who_is_it,
      content_tag(:span, t('who_are_you') ), '#', class: 'disabled',
      :unless => Proc.new { user_signed_in? }
    )
    primary.item(
      :key_user_login,
      content_tag(:span, t('login'), class:"label label-primary" ),
      new_user_session_path(),
      unless: Proc.new { user_signed_in? }
    )
    primary.item(
      :key_user_edit,
      (current_user.nil? ? '' : current_user.name), edit_user_registration_path(),
      if: Proc.new { user_signed_in? }
    )
    primary.item(
      :key_user_logout,
      content_tag( :span, t('logout'), class:"label label-primary" ),
      destroy_user_session_path(), method: Devise.sign_out_via,
      if: Proc.new { user_signed_in? }
    )

    primary.item( :key_separator2, '&nbsp;', '#', class: 'disabled' )
    primary.item( :key_locale,         content_tag(:span, t('language') ), '#'
    ) do |lev2_nav|
      lev2_nav.item :key_locale_it,    image_tag('it.png'), url_for( request.filtered_parameters.dup.merge('locale'=>'it') )
      lev2_nav.item :key_locale_en,    image_tag('us.png'), url_for( request.filtered_parameters.dup.merge('locale'=>'en') )
    end

    # you can also specify html attributes to attach to this particular level
    # works for all levels of the menu
    # primary.dom_attributes = {id: 'menu-id', class: 'menu-class'}
    primary.dom_class = 'nav navbar-nav'

    # You can turn off auto highlighting for a specific level
    # primary.auto_highlight = false
  end
end
