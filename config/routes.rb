Authtest::Application.routes.draw do

  devise_for :users

  # [Steve, 20130716] Root's route required by Devise:
  root to: "home#index", locale: /en|it/

  scope "/" do
    scope "(:locale)", locale: /en|it/ do
      # === Home ===
      get "index",  to: "home#index"
      get "wip",    to: "home#wip"

      resources :articles
    end
  end


  # Any other routes are handled here (since in Rails 3 ActionDispatch prevents
  # RoutingError from hitting ApplicationController::rescue_action).
  # In other words, this wildcard route will catch all the other cases:
  match "*path", to: "application#routing_error", via: [:get, :post, :put, :delete, :patch]


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
