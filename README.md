## *** AuthTest ***
### Test app for simple authorization log-in with Devise

[![PullReview stats](https://www.pullreview.com/bitbucket/steveoro/authtest/badges/master.svg?)](https://www.pullreview.com/bitbucket/steveoro/authtest/reviews/master)

[coverageImg]: https://bytebucket.org/steveoro/authtest/raw/a89c7d63536d4807c31c38002b2a9edfb863c2a3/coverage/coverage-badge.png "Test Coverage Badge"
![Test Coverage Badge][coverageImg]


#### Demo video:

[Short video of the running app](https://www.youtube.com/embed/4c_jV9CRl3E)


#### Description:
Simple log-in/authorization test App written in Rails 4 with Bootstrap 3 and RSpec 3 for TDD.

Supports multi-language localization and basic locale switching (currently only EN & IT).
Devise adds in-app User registration & confirmation.

Beyond the login phase there's nothing more than a RESTful CRUD "Articles" controller.
Test-driven developed in a little less than a day, just for fun & to test Bootstrap 3 basic features with Rails 4.
