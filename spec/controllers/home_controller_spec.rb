require 'spec_helper'


describe HomeController do
  include ControllerMacros
  render_views

  describe '[GET #index]' do
    context "(for an unlogged user)" do
      it "sucessfully handles the request" do
        get :wip
        expect(response.status).to eq(200)
      end
      it "renders the index template" do
        get :index
        expect(response).to render_template(:index)
      end
      it "renders the login path" do
        get :index
        expect(response.body).to include( new_user_session_path() )
      end
      it "renders the registration path" do
        get :index
        expect(response.body).to include( new_user_registration_path() )
      end
    end

    context "(for a logged-in user)" do
      login_user()
      it "sucessfully handles the request" do
        get :index
        expect(response.status).to eq(200)
      end
      it "renders the index template" do
        get :index
        expect(response).to render_template(:index)
      end
    end
  end
  # ===========================================================================

  describe '[GET #wip]' do
    context "(for any user)" do
      it "sucessfully handles the request" do
        get :wip
        expect(response.status).to eq(200)
      end
      it "renders the wip template" do
        get :wip
        expect(response).to render_template(:wip)
      end
    end
  end
  # ===========================================================================
end
