require 'spec_helper'


describe ArticlesController do
  include ControllerMacros
  render_views

  describe '[GET #index]' do
   context "(for an unlogged user)" do
     it "displays always the Login page" do
       get_action_and_check_if_its_the_login_page_for( :index )
     end
   end
    # -------------------------------------------------------------------------

    context "(for a logged-in user)" do
      login_user()

      it "successfully handles the request" do
        get :index
        expect(response.status).to eq(200)
      end
      it "assigns the required variables" do
        @article = create(:article)
        get :index
        expect( assigns(:articles) ).not_to be_nil 
      end
      it "lists the existing articles" do
        @article = create(:article)
        get :index
        expect( assigns(:articles) ).to respond_to(:each) 
        expect( assigns(:articles) ).to respond_to(:size) 
        expect( assigns(:articles).size > 0 ).to be_true 
        expect( assigns(:articles) ).to respond_to(:first) 
        expect( assigns(:articles).first ).to be_an_instance_of(Article) 
      end
      it "renders the index template" do
        get :index
        expect(response).to render_template(:index)
      end
    end
  end
  # ===========================================================================

  describe '[GET #show]' do
   context "(for an unlogged user)" do
     it "displays always the Login page" do
       get_action_and_check_if_its_the_login_page_for( :show, id: 0 )
     end
   end
    # -------------------------------------------------------------------------

    context "(for a logged-in user)" do
      before(:each) { @article = create(:article) }
      login_user()

      it "successfully handles the request" do
        get :show, id: @article.id
        expect(response.status).to eq(200)
      end
      it "assigns the required variables" do
        get :show, id: @article.id
        expect( assigns(:article) ).to be_an_instance_of(Article) 
      end
      it "renders the show template" do
        get :show, id: @article.id
        expect(response).to render_template(:show)
      end
      it "displays the article title" do
        get :show, id: @article.id
        expect(response.body).to include(@article.title)
      end
      it "displays the article content" do
        get :show, id: @article.id
        expect(response.body).to include(@article.content)
      end
    end
  end
  # ===========================================================================

  describe '[GET #new]' do
   context "(for an unlogged user)" do
     it "displays always the Login page" do
       get_action_and_check_if_its_the_login_page_for( :new )
     end
   end
    # -------------------------------------------------------------------------

    context "(for a logged-in user)" do
      login_user()

      it "successfully handles the request" do
        get :new
        expect(response.status).to eq(200)
      end
      it "assigns the required variables" do
        get :new
        expect( assigns(:article) ).to be_an_instance_of( Article ) 
      end
      it "renders the new template" do
        get :new
        expect(response).to render_template(:new)
      end
    end
  end
  # ===========================================================================

  describe '[POST #create]' do
    context "(for an unlogged user)" do
      it "displays always the Login page" do
        post :create, article: attributes_for(:article)
        expect(response).to redirect_to( '/users/sign_in' )
      end
    end
    # -------------------------------------------------------------------------

    context "(for a logged-in user)" do
      login_user()

      it "creates a new article" do
        expect{
          post :create, article: attributes_for(:article)
        }.to change{ Article.count }.by(1)
      end
      it "redirects to #show when successfull" do
        post :create, article: attributes_for(:article)
        expect(response).to redirect_to( article_path(Article.last) )
      end
      it "saves always the correct current user id" do
        post :create, article: attributes_for(:article, user_id: @user.id)
        expect(Article.last.user_id).to eq(@user.id)
      end
    end
  end
  # ===========================================================================

  describe '[GET #edit]' do
    before(:each) { @article = create(:article) }
    context "(for an unlogged user)" do
      it "displays always the Login page" do
        get_action_and_check_if_its_the_login_page_for( :edit, id: @article.id )
      end
    end
    # -------------------------------------------------------------------------

    context "(for a logged-in user)" do
      login_user()

      it "successfully handles the request" do
        get :edit, id: @article.id
        expect(response.status).to eq(200)
      end
      it "assigns the required variables" do
        get :edit, id: @article.id
        expect( assigns(:article) ).to be_an_instance_of( Article ) 
      end
      it "renders the edit template" do
        get :edit, id: @article.id
        expect(response).to render_template(:edit)
      end
    end
  end
  # ===========================================================================

  describe '[PUT #update]' do
    context "(for an unlogged user)" do
      before(:each) { @article = create(:article) }
      it "displays always the Login page" do
        put :update, id: @article.id, title: "New title"
        expect(response).to redirect_to( '/users/sign_in' )
      end
    end
    # -------------------------------------------------------------------------

    context "(for a logged-in user)" do
      login_user()
      before(:each) { @article = create(:article, user: @user) }

      it "redirects to #show when successfull" do
        put :update, id: @article.id, title: "New title"
        expect(response).to redirect_to( article_path(Article.last) )
      end
      it "saves always the correct current user id" do
        put :update, id: @article.id, title: "New title"
        @article.reload
        expect(@article.user_id).to eq(@user.id)
      end
    end
  end
  # ===========================================================================

  describe '[DELETE #destroy]' do
    before(:all) { @article = create(:article) }

    context "(for an unlogged user)" do
      it "does not delete an existing article" do
        expect{
          delete :destroy, id: @article.id
        }.not_to change{ Article.count }
      end
      it "displays always the Login page" do
        delete :destroy, id: @article.id
        expect(response).to redirect_to( '/users/sign_in' )
      end
    end
    # -------------------------------------------------------------------------

    context "(for a logged-in user)" do
      login_user()

      it "deletes an existing article" do
        expect{
          delete :destroy, id: @article.id
        }.to change{ Article.count }.by(-1)
      end
      it "redirects to #index when successfull" do
        delete :destroy, id: @article.id
        expect(response).to redirect_to( articles_path() )
      end
    end
  end
  # ===========================================================================
end
