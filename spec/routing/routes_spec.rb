require 'spec_helper'


describe "custom & named routes" do
  include CustomMatchers::Routing

  describe HomeController do
    it { should have_named_route :index, '/index' }
    it { should have_named_route :wip, '/wip' }
  end
  #-- -------------------------------------------------------------------------
  #++
end
#-- ---------------------------------------------------------------------------
#++


describe "RESTful routes" do
  describe ArticlesController do
    it "has a [GET]#index route" do
      expect( get: articles_path ).to route_to( controller: "articles", action: "index" )
    end
    it "has a [GET]#show route" do
      expect( get: article_path(id: 1) ).to route_to( controller: "articles", action: "show", id: '1' )
    end
    it "has a [GET]#new route" do
      expect( get: new_article_path ).to route_to( controller: "articles", action: "new" )
    end
    it "has a [POST]#create route" do
      article_attributes = attributes_for(:article)
      expect( post: 'articles', article: article_attributes ).to route_to( controller: "articles", action: "create" )
    end
    it "has a [GET]#edit route" do
      expect( get: edit_article_path(id: 1) ).to route_to( controller: "articles", action: "edit", id: '1' )
    end
    it "has a [PUT]#update route" do
      article_attributes = attributes_for(:article)
      expect( put: 'articles/1', article: article_attributes ).to route_to( controller: "articles", action: "update", id: '1' )
    end
    it "has a [DELETE]#destroy route" do
      expect( delete: 'articles/1' ).to route_to( controller: "articles", action: "destroy", id: '1' )
    end
  end
  #-- -------------------------------------------------------------------------
  #++
end
#-- ---------------------------------------------------------------------------
#++
