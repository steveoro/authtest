require 'spec_helper'


describe Article do
  context "[a well formed instance]" do
    before( :each ) do
      @article = create( :article )
    end

    subject { @article }

    it "is a valid istance" do
      expect( subject ).to be_valid
    end

    describe "(all required methods returning not-nil text)" do
      [
        :title, :content, :user_name
      ].each do |method_name|
        it "responds to ##{method_name}" do
          expect( subject ).to respond_to( method_name )
        end
        it "returns a String by invoking ##{method_name}" do
          expect( subject.send(method_name) ).to be_a( String )
        end
      end
    end
    # ---------------------------------------------------------------------------

    describe "#user_name" do
      it "includes the user name when a user is assigned to the article" do
        if subject.user
          expect( subject.user_name ).to include(subject.user.name)
        else
          expect( subject.user_name ).to include('?')
        end
      end
    end
    # --------------------------------------------------------------------------
  end
  # ----------------------------------------------------------------------------

end
