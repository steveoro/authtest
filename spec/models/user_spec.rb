require 'spec_helper'


describe User do
  context "[a well formed instance]" do
    before( :each ) do
      @user = create( :user )
    end

    subject { @user }

    it "is a valid istance" do
      expect( subject ).to be_valid
    end

    describe "(all required methods returning not-nil text)" do
      [
        :name, :description, :email,
        :get_full_name, :get_verbose_name, :to_s
      ].each do |method_name|
        it "responds to ##{method_name}" do
          expect( subject ).to respond_to( method_name )
        end
        it "returns a String by invoking ##{method_name}" do
          expect( subject.send(method_name) ).to be_a( String )
        end
      end
    end
    # ---------------------------------------------------------------------------

    describe "#get_full_name" do
      it "includes the user name" do
        expect( subject.get_full_name ).to include(subject.name)
      end
      it "includes the user description" do
        expect( subject.get_full_name ).to include(subject.description)
      end
    end

    describe "#get_verbose_name" do
      it "includes the user name" do
        expect( subject.get_verbose_name ).to include(subject.get_full_name)
      end
      it "includes the user e-mail" do
        expect( subject.get_verbose_name ).to include(subject.email)
      end
    end
    # --------------------------------------------------------------------------
  end
  # ----------------------------------------------------------------------------

end
