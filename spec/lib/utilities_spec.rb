require 'spec_helper'
require 'framework/utilities'


describe Utilities do
  it "responds to #strip_hash_members" do
    expect( subject ).to respond_to( :strip_hash_members )
  end
  # ---------------------------------------------------------------------------

  describe "#strip_hash_members" do
    it "returns an Hash instance" do
      expect( subject.strip_hash_members({}) ).to be_an_instance_of( Hash )
    end

    it "returns an Hash instance even if the parameter is nil" do
      expect( subject.strip_hash_members(nil) ).to be_an_instance_of( Hash )
    end

    it "strips the forbidden keys from the 0-th level hash depth" do
      hash = { id: 1, name: 'test', password: 'asdfg', password_confirmation: 'asdfg', note: 'whatever' }
      forbidden_keys = ['password', 'password_confirmation']
      result = subject.strip_hash_members( hash, forbidden_keys )
      forbidden_keys.each do |removed_key|
        expect( result.has_key?(removed_key) ).to be_false
      end
    end

    it "strips the forbidden keys from the 1-st level hash depth" do
      hash = { id: 1, action: 'create', user: { 'name' => 'steve', 'password' => 'asdfg', password_confirmation: 'asdfg', note: 'whatever' } }
      forbidden_keys = Utilities::DEFAULT_FORBIDDEN_KEYS
      result = subject.strip_hash_members( hash )
      hash.each do |key, value|
        if value.instance_of?( Hash )
          forbidden_keys.each do |removed_key|
            expect( result.has_key?(removed_key) ).to be_false
          end
        end
      end
    end
  end
  # ---------------------------------------------------------------------------
end
