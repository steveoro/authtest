require 'ffaker'


FactoryGirl.define do

  factory :article do
    sequence( :title )    { |n| "Great dummy article title n.#{n}" }
    content               { "#{Faker::Lorem.paragraph}\r\n#{Faker::Lorem.paragraph}" }
    user
  end
  # ---------------------------------------------------------------------------

end
