class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :lock_version, default: 0
      t.string :title, null: false, limit: 150
      t.text :content, null: false

      t.references :user
      t.timestamps
    end

    add_index :articles, :title
    add_index :articles, :user_id
  end
end
