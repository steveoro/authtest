source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.1'
gem 'activerecord-session_store'

gem 'mysql2'
gem 'json'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  # Use SCSS for stylesheets
  gem 'sass-rails', '~> 4.0.3'
  # Use CoffeeScript for .js.coffee assets and views
  gem 'coffee-rails', '~> 4.0.0'
  # Use Uglifier as compressor for JavaScript assets
  gem 'uglifier', '>= 1.3.0'

#  gem "twitter-bootstrap-rails", branch: 'bootstrap3', git: 'git://github.com/seyhunak/twitter-bootstrap-rails.git'
end

#gem 'execjs'                                        # This requires a local (package) install of node.js
#gem 'therubyracer', :platform => :ruby               # This seems to be the only one feasible on the EC2 micro instance
gem 'therubyrhino', :platforms => :ruby


# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use debugger
# gem 'debugger', group: [:development, :test]

gem "better_errors",      group: :development
gem "binding_of_caller",  group: :development


group :test do
  gem "rspec"
  gem "rspec-rails"
  gem 'cucumber'
  gem "capybara"                                    # [Steve, 20140226] Used only in Feature Specs
  gem "factory_girl_rails"
  gem 'guard'
  gem 'guard-rspec'
  gem 'rspec_api_blueprint', require: false
                                                    # Test coverage stats reports
  gem 'simplecov', '~> 0.7.1', require: false
  gem 'simplecov-badge', require: false
#  gem "codeclimate-test-reporter", require: false   # CI/Test coverage via local test run
  gem 'coveralls', require: false                   # [Steve, 20140312] Uses simplecov to obtain test-coverage badge

  # For using this one, keep in mind http://rubydoc.info/gems/faker/1.3.0/frames
  gem 'ffaker', require: false                      # Adds dummy names & fixture generator 
end

# Authentication:
gem 'devise'
gem 'devise-i18n'
gem 'simple_token_authentication'
gem 'haml'

gem "simple-navigation"
gem 'simple-navigation-bootstrap'

gem 'inherited_resources'
gem 'simple_form'
gem 'country_select'
gem "wice_grid"


# Deployment:
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
#  gem 'capistrano-rvm', group: :development
#  gem 'capistrano-bundler', '~> 1.1.2', group: :development
#  gem 'capistrano-rails', '~> 1.1', group: :development

# Deploy on Open-shift:
# gem 'rhc'                                           # [Steve, 20140426] For deployment to OpenShift (RedHat)
