require 'framework/utilities'


=begin
= ApplicationController

  - version:  1.00.001
  - author:   Steve A.

  Main Application controller.
=end
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale

# XXX Comment/Uncomment this to show or skip the 'better-errors' output page with stack trace:
  rescue_from Exception, with: :handle_exception
  rescue_from ActionController::RoutingError, with: :render_not_found

  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :user_registration_path, :log_registration, only: [:destroy]
  after_filter  :user_registration_path, :log_registration, only: [:create]


  # Set the default URL options:
  def default_url_options( options={} )
    logger.debug "default_url_options has options: #{options.inspect}\n"
    { locale: I18n.locale }
  end


  # Invoked for any 404/not found request: throws a custom exception
  # (catchable by this controller)
  def routing_error
    raise ActionController::RoutingError.new(params[:path])
  end
  #-- -------------------------------------------------------------------------
  #++


  protected


  # Whitelist additional parameters for Devise, "the lazy way".
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:name, :email) }
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :description, :password, :password_confirmation) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :description, :password, :password_confirmation, :current_password) }
  end


  # Just logs the specified output message using either WARN or ERROR level logging,
  # with no subsequent redirection being made.
  #
  # When an exception has been intercepted and the variable $! has been set,
  # the level will be set to ERROR (defaults to WARN).
  #
  def log_error( exception_or_text_msg, verbose_trace = true )
    if exception_or_text_msg.instance_of?( String )
      msg = "[W!]-- #{exception_or_text_msg}"
      logger.warn( msg )
    else
      output_message = exception_or_text_msg.respond_to?( :message ) ? exception_or_text_msg.message : exception_or_text_msg.to_s
      msg = "[E!]-- ERROR INTERCEPTED: #{output_message}"
      error_description = $!
      error_trace = exception_or_text_msg.respond_to?( :backtrace ) ? exception_or_text_msg.backtrace : '(backtrace not available)'
      user  = current_user() ? current_user().get_full_name : '(user not logged in yet)'
      logger.error( msg )
      logger.error( error_description )
                                                    # Send a message to the developers anyway:
      begin
        AgexMailer.exception_mail( nil, user, error_description, error_trace ).deliver
        logger.info("[*I*]- e-mail error report allegedly sent.")
      rescue
        logger.warn( '[W!]-- Unable to send out notification e-mail message, Mailer not responding or not configured properly yet.' )
      end
      logger.error( error_trace ) if verbose_trace
    end
  end


  # Sends out an e-mail to notify the developers of important actions by the users
  # taking place during app usage.
  #
  def log_action( action_performed, action_description )
    user = current_user()
    user = user ? user.get_full_name : '(user not logged in yet)'
    logger.info("[*I*]- ACTION: '#{action_performed}', current user: #{user} => #{action_description}.")
                                                  # Send a message to the developers anyway:
    begin
      AgexMailer.action_notify_mail( user, action_performed, action_description ).deliver
      logger.info("[*I*]- action-notify e-mail allegedly sent.")
    rescue
      logger.warn( '[W!]-- Unable to send out action-notify e-mail message, Mailer not responding or not configured properly yet.' )
    end
  end
  #-- -------------------------------------------------------------------------
  #++


  private


  # Handler for a simple 404 custom message or page.
  # (Renders a custom 404 page for a not-found route.)
  def render_not_found
    redirect_to wip_path()
  end

  # Handler for all other Exceptions/Errors
  def handle_exception( exception )
    log_error( exception, true )                    # (use verbose trace)
    flash[:error] = I18n.t(:something_went_wrong)
    # (Note that in order to avoid an infinite redirect loop is mandatory that the below path
    #  does NOT raise any exception)
    redirect_to root_path and return
  end
  #-- -------------------------------------------------------------------------
  #++


  # Logs registration actions with an admin email.
  def log_registration
    return unless request.filtered_parameters['controller'] == 'devise/registrations'

    if request.post?                                # === POST: ===
      log_action(
        "new User signed-up!",
        "Params: #{Utilities.strip_hash_members( params ).inspect}\r\n\r\nUpdated users total: #{User.count}\r\n\r\nCurrent user instance: #{current_user.inspect}"
      )
    elsif request.delete?                           # === DELETE: ===
      log_action(
        "existing User deleted!",
        "Params: #{Utilities.strip_hash_members( params ).inspect}\r\n\r\nUpdated users total: #{User.count}\r\n\r\nCurrent user instance: #{current_user.inspect}"
      )
    end
  end
  #-- -------------------------------------------------------------------------
  #++


  # Safe #to_sym conversion to avoid DOS-attacks by creating ludicrous amounts of Symbols.
  def to_safe_sym( value, valid_values )
    symbolized = nil
    valid_values.each do |v|
      if v == value
         symbolized = v.to_sym
        break
      end
    end
    symbolized
  end

  # Set current locale either from URL +locale+ parameter or from the default I18n value
  #
  # Will set the cookie 'locale' if (and only if) an explicit parameter 'locale'
  # is passed (and is acceptable) or it is explicitly specified by the locale part of the URL.
  # (Check out routes.rb for locale detection inside the URL request)
  #
  # - If no parameter or URL part specifies the locale, the cookie is used (if it exists)
  # - If no cookie exists, we look through the list of desired languages for the
  #   first one we can accept.
  #
  def set_locale
#    logger.debug "* Locale currently is '#{I18n.locale}', params[:locale] = '#{params[:locale]}'"
#    logger.debug "* cookies[:locale] = '#{cookies[:locale]}', HTTP_ACCEPT_LANGUAGE: '#{request.env["HTTP_ACCEPT_LANGUAGE"]}'"
    accept_locales = LOCALES.keys                   # See config/application.rb for accepted LOCALES

    locale = params[:locale] if accept_locales.include?( params[:locale] )
    if locale.nil?                                  # Use the cookie only when set or enabled
      locale = cookies[:locale] if accept_locales.include?( cookies[:locale] ) 
    else                                            # Store the chosen locale when it changes
      cookies[:locale] = locale
    end

    current_locale = locale || I18n.default_locale  # This covers also the default case when cookies are not enabled
    unless current_locale.nil?
      I18n.locale = to_safe_sym( current_locale, accept_locales )
      logger.debug "* Locale is now set to '#{I18n.locale}'"
    end
  end
  #-- -------------------------------------------------------------------------
  #++
end
