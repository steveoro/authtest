=begin

= HomeController

  - version:  1.00.001
  - author:   Steve A.

  Welcome page / Home controller. Not much to see here, move along...
=end
class HomeController < ApplicationController
  def index
  end

  def wip
  end
end
