=begin

= ArticlesController

  - version:  1.00.001
  - author:   Steve A.

  Articles REST controller, DRY'ed-up with InheritedResources.
=end
class ArticlesController < InheritedResources::Base

  respond_to :html

  # Require authorization before invoking any of this controller's actions:
  before_filter :authenticate_user!                # Devise "standard" HTTP log-in strategy
  #-- -------------------------------------------------------------------------
  #++


  protected


  def permitted_params
    params.permit( article: [:title, :content, :user_id] )
  end
  #-- -------------------------------------------------------------------------
  #++
end
