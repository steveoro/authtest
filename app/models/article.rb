=begin

= Article

  - version:  1.00.001
  - author:   Steve A.

  Article model class.
=end
class Article < ActiveRecord::Base
  belongs_to :user                                  # [Steve, 20120212] Do not validate associated user!

  validates_presence_of :title, length: { within: 1..150 }, allow_nil: false
  validates_presence_of :content, allow_nil: false 

  scope :sort_by_user,  ->(dir) { order("users.name #{dir.to_s}, articles.created_at #{dir.to_s}") }
  #-- -------------------------------------------------------------------------
  #++

  # Retrieves the user name associated with this article
  def user_name
    self.user.nil? ? '?' : self.user.name
  end
  #-- -------------------------------------------------------------------------
  #++
end
