=begin

= User

  - version:  1.00.001
  - author:   Steve A.

  User model class.
=end
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :confirmable, :lockable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of   :name
  validates_uniqueness_of :name, message: :already_exists

  validates_length_of     :description,   maximum: 100
  #-- -------------------------------------------------------------------------
  #++

  # Computes a descriptive name associated with this data
  def get_full_name
    "#{name} (#{description})"
  end

  # Computes a 'full debug data dump' associated with this data
  def get_verbose_name
    "#{get_full_name}, e-mail: #{email}"
  end

  # to_s() override for debugging purposes:
  def to_s
    "[User: '#{get_full_name}']"
  end
  #-- -------------------------------------------------------------------------
  #++

end
